require AdventHelpers

defmodule DayNine do
  def part_one(list) do
    prev = Enum.take(list, 25)
    num = Enum.at(list, 25)
    case check(prev, num) do
      false -> num
      true -> part_one(Enum.drop(list, 1))
    end
  end

  def part_two(input), do: part_two(input, part_one(input))

  def part_two(input, key) do
    windows = Enum.map(
      0..(length(input) - 1),
      &(Enum.slice(input, 0..&1))
    ) |> Enum.map(&({&1, Enum.sum(&1)}))
      |> Enum.find(fn {_, sum} -> sum == key end)
    case windows do
      nil -> part_two(Enum.drop(input, 1), key)
      {x, _} -> s = Enum.sort(x); List.last(s) + List.first(s)
    end
  end

  def check(prev, num) do
    # if 2 numbers were repeated, this would break.
    length(Enum.uniq(Enum.map(prev, &(abs(num - &1))) ++  prev)) != 50
  end
end

{:ok, content} = File.read("input9.txt")

input = AdventHelpers.parse_intlist(content)

IO.puts("Part 1: #{DayNine.part_one(input)}")
IO.puts("Part 2: #{DayNine.part_two(input)}")
