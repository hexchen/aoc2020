require AdventHelpers

defmodule DaySix do
  def part_one(input) do
    input
      |> Enum.map(fn x -> List.foldl(x, MapSet.new(), &MapSet.union/2) end)
      |> Enum.map(&MapSet.to_list/1)
      |> Enum.map(&length/1)
      |> List.foldl(0, &+/2)
  end

  def part_two(input) do
    input
                                        # ugly hack, we can't start from an empty set this time
      |> Enum.map(fn x -> List.foldl(x, List.first(x), &MapSet.intersection/2) end)
      |> Enum.map(&MapSet.to_list/1)
      |> Enum.map(&length/1)
      |> List.foldl(0, &+/2)
  end
end

{:ok, content} = File.read("input6.txt")

input = content
        |> String.split("\n\n")
	|> Enum.filter(&(&1 != ""))
	|> Enum.map(fn x -> String.split(x, "\n") |> Enum.map(&String.codepoints/1) end)
	|> Enum.map(fn x -> Enum.filter(x, &(&1 != [])) end)
	|> Enum.map(fn x -> Enum.map(x, &MapSet.new/1) end)

IO.puts("Part 1: #{DaySix.part_one(input)}")
IO.puts("Part 2: #{DaySix.part_two(input)}")
