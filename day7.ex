require AdventHelpers

defmodule DaySeven do
  def part_one(input) do
    input
      |> Map.keys
      |> Enum.filter(&(contains(input, "shiny gold", {&1, Map.get(input, &1)})))
      |> length
  end

  def part_two(input) do
    childcount(input, "shiny gold")
  end

  def contains(all, search, {bag, children}) do
    colors = Enum.map(children, fn {color, _count} -> color end)
    case Enum.any?(colors, &(&1 == search)) do
      true  -> true
      false -> Enum.map(colors, &({&1, Map.get(all, &1)}))
                 |> Enum.any?(&(contains(all, search, &1)))
    end
  end

  def childcount(all, bag) do
    Map.get(all, bag)
      |> Enum.map(fn {col, count} -> count  * (1 + childcount(all, col)) end)
      |> List.foldl(0, &+/2)
  end

  def parse_line(line) do
    [ color, rest ] = String.split(line, " bags contain ")
    children = rest
      |> String.split(", ")
      |> Enum.filter(&(&1 != ""))
      |> Enum.map(&parse_bag/1)
      |> Enum.filter(&(&1 != {}))
    { color, children }
  end
  
  def parse_bag(bagstr) do
    regex = ~r/(((\d+) ([a-z ]+) bag[s]?)|(no other bags))/
    case Regex.scan(regex, bagstr) do
      [[ _match, _g1, _g2, count, color ]] -> {color, Integer.parse(count) |> Tuple.to_list |> Enum.at(0)}
      [[ _match, _g1, _g2, _count, _color, "no other bags" ]] -> {}
    end
  end
end

{:ok, content} = File.read("input7.txt")

input = content 
        |> String.split("\n")
        |> Enum.filter(&(&1 != ""))
        |> Enum.map(&DaySeven.parse_line/1)
        |> Map.new

IO.puts("Part 1: #{DaySeven.part_one(input)}")
IO.puts("Part 2: #{DaySeven.part_two(input)}")
