require AdventHelpers

defmodule DayTwo do
  def part_one(input) do
    input
      |> Enum.filter(&check/1)
      |> Enum.count
  end

  def part_two(input) do
    input
      |> Enum.filter(&checknew/1)
      |> Enum.count
  end

  def check({lower, upper, char, passwd}) do
    count = (String.split(passwd, char) |> Enum.count) - 1
    count >= lower and count <= upper
  end

  def checknew({pos1, pos2, char, passwd}) do
    cp = String.codepoints(passwd)
    p1 = Enum.at(cp, pos1 - 1) == char
    p2 = Enum.at(cp, pos2 - 1) == char
    (p1 or p2) and not (p1 and p2)
  end

  def parse_line([lower, upper, char, passwd]) do
    {lwr, _} = Integer.parse(lower)
    {upr, _} = Integer.parse(upper)
    {lwr, upr, char, passwd}
  end

  def parse_line(line) do
    pattern = ~r/(\d+)-(\d+) (.): (.+)/
    [[_ | match]] = Regex.scan(pattern, line)
    parse_line(match)
  end
end

{:ok, content} = File.read("input2.txt")

input = content 
        |> String.split("\n")
	|> Enum.filter(&(&1 != ""))
	|> Enum.map(&DayTwo.parse_line/1)

IO.puts("Part 1: #{DayTwo.part_one(input)}")
IO.puts("Part 2: #{DayTwo.part_two(input)}")
