require AdventHelpers

defmodule DayEight do
  def part_one(input) do
    { :loop, acc} = input
      |> exec_repeat(0, 0)
    acc
  end

  def part_two(input) do
    inverted = Enum.map(
      0..(length(input) - 1), 
      fn c -> List.replace_at(input, c, invert Enum.at(input, c)) end
    )
    { :done, acc } = inverted
      |> Enum.uniq
      |> Enum.map(&(exec_repeat(&1, 0, 0)))
      |> Enum.find(fn {code, _} -> code == :done end)
    acc
  end

  def invert({"nop", arg, v}), do: {"jmp", arg, v}
  def invert({"jmp", arg, v}), do: {"nop", arg, v}
  def invert(x), do: x

  def exec_repeat(prog, acc, pos) when pos == length(prog), do: { :done, acc}
  def exec_repeat([ {_, _, true} | _ ], acc, _), do: { :loop, acc }
  def exec_repeat([ {"jmp", arg, _} | rem ], acc, pos), do: exec_repeat(rotate([{"jmp", arg, true} | rem], arg), acc, pos + arg)
  def exec_repeat([ {"nop", arg, _} | rem ], acc, pos), do: exec_repeat(rotate([{"nop", arg, true} | rem], 1), acc, pos + 1)
  def exec_repeat([ {"acc", arg, _} | rem ], acc, pos), do: exec_repeat(rotate([{"nop", arg, true} | rem], 1), acc + arg, pos + 1)

  def rotate(list, 0), do: list
  def rotate(list, count) when count < 0, do: rotate(list, length(list) + count)
  def rotate(list, count) when count > 0 do
    Stream.cycle(list)
      |> Stream.drop(count)
      |> Stream.take(length(list))
      |> Enum.to_list
  end

  def parse_line(line) do
    [ op, args ] = String.split(line)
    { arg, _ } = Integer.parse(args)
    { op, arg, false }
  end
end

{:ok, content} = File.read("input8.txt")

input = content 
        |> String.split("\n")
        |> Enum.filter(&(&1 != ""))
        |> Enum.map(&DayEight.parse_line/1)

IO.puts("Part 1: #{DayEight.part_one(input)}")
IO.puts("Part 2: #{DayEight.part_two(input)}")
