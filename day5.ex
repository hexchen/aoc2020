require AdventHelpers

defmodule DayFive do
  def part_one(input) do
    seats(input)
      |> Enum.sort
      |> List.last
  end

  def part_two(input) do
    all_seats = MapSet.new(0..part_one(input))
    existing_seats = MapSet.new(seats(input))
    MapSet.difference(all_seats, existing_seats)
      |> Enum.filter(&(Bitwise.bsr(&1, 3) > 0))
      |> Enum.filter(&(Bitwise.bsr(&1, 3) < 127))
      |> Enum.filter(&(MapSet.member?(existing_seats, &1 + 1) and MapSet.member?(existing_seats, &1 - 1)))
      |> List.first
  end

  def seats(input) do
    input
      |> Enum.map(&seat_id/1)
  end

  def seat_id({vert, hori}) do
    decode(vert, 0) * 8 + decode(hori, 0)
  end

  def decode([], acc), do: acc
  def decode([ "F" | tail ], acc), do: decode(tail, Bitwise.bsl(acc, 1) |> Bitwise.bor(0))
  def decode([ "B" | tail ], acc), do: decode(tail, Bitwise.bsl(acc, 1) |> Bitwise.bor(1))
  def decode([ "L" | tail ], acc), do: decode(tail, Bitwise.bsl(acc, 1) |> Bitwise.bor(0))
  def decode([ "R" | tail ], acc), do: decode(tail, Bitwise.bsl(acc, 1) |> Bitwise.bor(1))
end

{:ok, content} = File.read("input5.txt")

input = content
        |> String.split("\n")
	|> Enum.filter(&(&1 != ""))
	|> Enum.map(&String.codepoints/1)
	|> Enum.map(&(Enum.split(&1, 7)))

IO.puts("Part 1: #{DayFive.part_one(input)}")
IO.puts("Part 2: #{DayFive.part_two(input)}")
