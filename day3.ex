require AdventHelpers

defmodule DayThree do
  def part_one(input) do
    move(input, 3, 1, 0)
  end

  def part_two(input) do
    [ move(input, 1, 1, 0),
      move(input, 3, 1, 0),
      move(input, 5, 1, 0),
      move(input, 7, 1, 0),
      move(input, 1, 2, 0) ]
      |> List.foldl(1, &*/2)
  end

  def move([], _, _, acc) do
    acc
  end

  def move(input, dx, dy, acc) do
#   IO.inspect(Enum.at(input, 0) |> println |> List.to_string)
    over = Enum.drop(input, dy)
      |> Enum.map(&Stream.cycle/1)
      |> Enum.map(&(Stream.drop(&1, dx)))
      |> Enum.map(&(Stream.take(&1, 31)))
      |> Enum.map(&Enum.to_list/1)
    case over do
      [ [ true | _ ] | _ ] -> move(over, dx, dy, acc + 1)
      _ -> move(over, dx, dy, acc)
    end
  end

  def println(line) do
    Enum.map(line, &(case &1 do
      true -> "X"
      false -> "."
    end))
  end
end

{:ok, content} = File.read("input3.txt")

input = content
        |> String.split("\n")
	|> Enum.filter(&(&1 != ""))
	|> Enum.map(&String.codepoints/1)
	|> Enum.map(&(Enum.map(&1, fn x -> x == "#" end)))

IO.puts("Part 1: #{DayThree.part_one(input)}")
IO.puts("Part 2: #{DayThree.part_two(input)}")
