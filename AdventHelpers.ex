defmodule AdventHelpers do
  def parse_intlist(input) do
    String.split(input, "\n")
      |> Enum.filter(&(&1 != ""))
      |> Enum.map(&String.to_integer/1)
  end
end
