require AdventHelpers

defmodule DayOne do
  def part_one(input) do
    [ {2020, a, b} | _ ] = Enum.map(input, fn x -> Enum.map(input, fn y -> {x + y, x, y} end) end)
      |> List.flatten
      |> Enum.filter(fn {sum, _, _} -> sum == 2020 end)
    a * b
  end

  def part_two(input) do
    [ {2020, a, b, c} | _ ] = Enum.map(input, fn x -> Enum.map(input, fn y -> Enum.map(input, fn z -> {x + y + z, x, y, z} end) end) end)
      |> List.flatten
      |> Enum.filter(fn {sum, _, _, _} -> sum == 2020 end)
    a * b * c
  end
end

{:ok, content} = File.read("input1.txt")

input = AdventHelpers.parse_intlist(content)

IO.puts("Part 1: #{DayOne.part_one(input)}")
IO.puts("Part 2: #{DayOne.part_two(input)}")
