require AdventHelpers

defmodule DayFour do
  def part_one(input) do
    input
      |> Enum.filter(&has_fields/1)
      |> length
  end

  def part_two(input) do
    input
      |> Enum.filter(&has_fields/1)
      |> Enum.filter(&valid/1)
      |> length
  end

  def has_fields(passport) do
    [ "byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid" ]
      |> Enum.map(&(Map.has_key?(passport, &1)))
      |> Enum.all?(&(&1))
  end

  def valid(%{
    "byr" => byrs,
    "iyr" => iyrs,
    "eyr" => eyrs,
    "hgt" => hgts,
    "hcl" => hcl,
    "ecl" => ecl,
    "pid" => pids
  }) do
    ints = [ byrs, iyrs, eyrs, pids ]
     |> Enum.map(&Integer.parse/1)
    if (not Enum.any?(ints, &(&1 == :error))) and (not Enum.any?(ints, fn {_, x} -> x != "" end)) do
      [ byr, iyr, eyr, _pid ] = ints |> Enum.map(fn {x, _} -> x end)
      byrv = byr >= 1920 and byr <= 2020
      iyrv = iyr >= 2010 and iyr <= 2020
      eyrv = eyr >= 2020 and eyr <= 2030
      pidv = length(String.codepoints(pids)) == 9
      hgtv = case Integer.parse(hgts) do
        {hgt, "cm"} when hgt >= 150 and hgt <= 193 -> true
        {hgt, "in"} when hgt >= 59 and hgt <= 76 -> true
        _ -> false
      end
      hclv = Regex.match?(~r/#[a-f0-9]{6}/, hcl)
      eclv = [ "amb", "blu", "brn", "gry", "grn", "hzl", "oth" ]
        |> Enum.any?(&(&1 == ecl))
      [ byrv, iyrv, eyrv, pidv, hgtv, hclv, eclv ]
        |> Enum.all?(&(&1))
    else
      false
    end
  end
end

{:ok, content} = File.read("input4.txt")

input = content
        |> String.split("\n\n")
	|> Enum.filter(&(&1 != ""))
	|> Enum.map(&String.split/1)
	|> Enum.map(&(Enum.map(&1, fn x -> String.split(x, ":") |> List.to_tuple end)))
	|> Enum.map(&Map.new/1)

IO.puts("Part 1: #{DayFour.part_one(input)}")
IO.puts("Part 2: #{DayFour.part_two(input)}")
